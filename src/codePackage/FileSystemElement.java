package codePackage;

public abstract class FileSystemElement {
    String elemName;

    abstract void printHeader(int indent);

    abstract FileSystemElement copy();

    void store(String commit) {
        Storage.getInstance().map.put(commit, this.copy());
    }

    abstract void restore(String commit);
}
