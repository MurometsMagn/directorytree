package codePackage;

//do not delete: use to check file system
public class PrintDir {
    public static String printDirectoryTree(Directory root) {

        int indent = 0;
        StringBuilder sb = new StringBuilder();
        printDirectoryTree(root, indent, sb);
        return sb.toString();
    }

    private static void printDirectoryTree(Directory folder, int indent, StringBuilder sb) {
        sb.append(getIndentString(indent));
        sb.append("+--");
        sb.append(folder.elemName);
        sb.append("/");
        sb.append("\n");

        for (FileSystemElement file : folder.list) {
            if (file instanceof  Directory) {
                printDirectoryTree((Directory) file, indent + 1, sb);
            } else {
                file.printHeader(indent + 1);
                printFile((File) file, indent + 1, sb);
            }
        }
    }

    private static void printFile(File file, int indent, StringBuilder sb) {
        sb.append(getIndentString(indent));
        sb.append("+--");
        if (file != null) {
            sb.append(file.elemName);
        } else {
            sb.append("null file name");
        }
        sb.append("\n");
    }

    private static String getIndentString(int indent) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            sb.append("|  ");
        }
        return sb.toString();
    }
}
