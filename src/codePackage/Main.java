package codePackage;

/**
 * Created by Smolentsev Il'ya on 08.03.2020.
 */
public class Main {
    public static void main(String[] args) {
        final Directory dir1 = new Directory("dir1");
        final Directory dir2 = new Directory("dir2");

        final Directory root = new Directory("root");
        root.addElem(dir1);
        root.addElem(dir2);

        final File file1 = new File("file1");
        final File file2 = new File("file2");
        dir1.addElem(file1);
        dir2.addElem(file2);

        //System.out.println(PrintDir.printDirectoryTree(root));

        root.printHeader(0);

        System.out.println("second part");
       // root.copy();

        root.store("root");
        Directory root2 = new Directory();
        root2.restore("root");
        root2.printHeader(0);
    }
}
