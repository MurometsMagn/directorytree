package codePackage;

public class File extends FileSystemElement {
    public File(String fileName) {
        this.elemName = fileName;
    }

    @Override
    public void printHeader(int indent) {
        for (int i = 0; i <= indent; i++) {
            System.out.print('\t');
        }
        System.out.println(elemName);
    }

    @Override
    FileSystemElement copy() {
        final File result = new File(elemName);
        return result;
    }

    @Override
    void restore(String commit) {
        File f = (File) Storage.getInstance().map.get(commit);
        elemName = f.elemName;
    }
}
