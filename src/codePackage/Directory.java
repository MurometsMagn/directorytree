package codePackage;

import java.util.ArrayList;
import java.util.List;

public class Directory extends FileSystemElement {
    List<FileSystemElement> list = new ArrayList<>();

    public Directory(String dirName) {
        this.elemName = dirName;
    }

    public Directory() {
    }

    public void addElem(FileSystemElement element) {
        list.add(element);
    }

    @Override
    void printHeader(int indent) {
        for (int i = 0; i <= indent; i++) {
            System.out.print('\t');
        }
        System.out.println(elemName + '/');
        for (FileSystemElement fileSystemElement : list) {
            fileSystemElement.printHeader(indent + 1);
        }
    }

    @Override
    FileSystemElement copy() {
        final Directory dir = new Directory(elemName);
        dir.list = new ArrayList<>();
        for (FileSystemElement i : list) {
            dir.list.add(i.copy());
        }
        printHeader(0);
        return dir;
    }

    @Override
    void restore(String commit) {
        Directory elem = (Directory) Storage.getInstance().map.get(commit);
        elemName = elem.elemName;
        list.clear();
        for (FileSystemElement i : elem.list) {
            list.add(i.copy());
        }
    }
}
