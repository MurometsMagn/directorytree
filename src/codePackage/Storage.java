package codePackage;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Smolentsev Il'ya on 14.03.2020.
 */

//caretaker
public class Storage { //singleton
    //мапа(коммит, рут):
    Map<String, FileSystemElement> map = new HashMap<>();

    public Map<String, FileSystemElement> getMap() {
        return map;
    }

    public void setMap(Map<String, FileSystemElement> map) {
        this.map = map;
    }
    private static Storage instance = new Storage();

    public static Storage getInstance() {
        return instance;
    }

    private Storage() {
    }
}
