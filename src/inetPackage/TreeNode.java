package inetPackage;

import java.io.File;

//I am using this one!
//https://stackoverflow.com/questions/10655085/print-directory-tree

public class TreeNode {
    public static void main(String[] args) {
        //final String fname = "C:/Users/Муромец/Desktop/basura";
        //final String fname = "~/";//домашняя папка
        final String fname = "/home/boltstudent/Рабочий стол/SmolentsevIlya";
        System.out.println(printDirectoryTree(fname));
    }

    public static String printDirectoryTree(String path) {
        File folder = new File(path);
        if (!folder.isDirectory()) {
            throw new IllegalArgumentException("folder is not a Directory");
        }
        int indent = 0; //отступ
        StringBuilder sb = new StringBuilder();
        printDirectoryTree(folder, indent, sb);
        return sb.toString();
    }

    private static void printDirectoryTree(File folder, int indent, StringBuilder sb) {

        if (folder != null && folder.isDirectory() && folder.listFiles() != null) {
            sb.append(getIndentString(indent));
            sb.append("+--");
            sb.append(folder.getName());
            sb.append("/");
            sb.append("\n");

            for (File file : folder.listFiles()) {
                if (file.isDirectory()) {
                    printDirectoryTree(file, indent + 1, sb);
                } else {
                    printFile(file, indent + 1, sb);
                }
            }
        }
    }

    private static void printFile(File file, int indent, StringBuilder sb) {
        sb.append(getIndentString(indent));
        sb.append("+--");
        if (file != null) {
            sb.append(file.getName());
        } else {
            sb.append("null file name");
        }
        sb.append("\n");
    }

    private static String getIndentString(int indent) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            sb.append("|  ");
        }
        return sb.toString();
    }
}
